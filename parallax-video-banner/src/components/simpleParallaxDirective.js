import simpleParallax from 'simple-parallax-js';
export default {
    mounted(el, _a) {
        el.instanceMap = new WeakMap();
        var value = _a.value;
        if (value === false) {
            return;
        }
        el.instanceMap.set(el, new simpleParallax(el, value));
    },
    updated(el, _a) {
        var value = _a.value;
        var instance = el.instanceMap.get(el);
        if (!instance) {
            return;
        }
        instance.destroy();
        if (value === false) {
            return;
        }
        el.instanceMap.set(el, new simpleParallax(el, value));
    },
    unmounted(el) {
        var instance = el.instanceMap.get(el);
        if (!instance) {
            return;
        }
        instance.destroy();
    },
};