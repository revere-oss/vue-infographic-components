# parallax-video-banner

This is a full width banner component with an embedded muted, auto-playing video which utilises the 'simpleParallax' to provide a parallax effect on scroll.

## DEMO

This project has a built-in demo, simply run `npm run serve` to view it.

## Installation

```
import ParallaxVideoBanner from "@revere/parallax-video-banner";
```

## Usage

```
<ParallaxVideoBanner
			class="[add any class]"
			src="[path to video file]"
			:options="{ simpleParallax options object. See simpleparallax.com }"
		/>
```

## Publishing component updates

- Make changes
- Increment version number in package.json
- `npm publish`

---

### Usual Commands for VUE CLI:

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
