# two-figure-ticker

## DEMO

This project has a built-in demo, simply run `npm run serve` to view it.

## Installation

```
import TwoFigureTicker from "@revere-oss/two-figure-ticker";
```

## Usage

```
<TwoFigureTicker
    :left-number="[number - max: 0]"
    :right-number="[number - min: 0]"
    :duration="[millis - def: 800]"
    :startOnVisible="[true/false - def: false]"
/>
```

## Publishing component updates

-   Make changes
-   Increment version number in package.json
-   `npm publish`

---

### Usual Commands for VUE CLI:

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
