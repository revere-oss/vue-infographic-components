# horizontal-progress-view

![Preview](https://i.imgur.com/NU55rMF.png)

This is a customisable animated horizontal progress bar with text label. It is visiblity-aware and can animate on becoming visible in the viewport.

## DEMO

This project has a built-in demo, simply run `npm run serve` to view it.

## Installation

```
import HorizontalProgressView from "@revere/horizontal-progress-view";
```

## Usage

```
<HorizontalProgressView
    :value="[Number]"
    :show-text="[true/false]"
    :duration="[Number - millis]"
    :animate-on-visible="[true/false]"
    bar-color="[hex code]"
    background-color="[hex code]"
    text-color="[hex code]"
/>
```

## Publishing component updates

- Make changes
- Increment version number in package.json
- `npm publish`

---

### Usual Commands for VUE CLI:

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
