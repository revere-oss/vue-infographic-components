# circle-progress-view

![Preview](https://i.imgur.com/QV7p4wl.png)

This is a highly customisable animated circular progress bar with text label. It is visiblity-aware and can animate on becoming visible in the viewport.

See https://www.npmjs.com/package/vue3-circle-progress for more information on the progress circle from which this is based.

## DEMO

This project has a built-in demo, simply run `npm run serve` to view it.

## Installation

```
import CircleProgressView from "@revere/circle-progress-view";
```

## Usage

```
<CircleProgressView
    class="[add any class]"
    :value="[number value]"
    :animate-on-visible="[true/false]"
/>
```

You may also pass any props that are compatible with https://www.npmjs.com/package/vue3-circle-progress

## Publishing component updates

- Make changes
- Increment version number in package.json
- `npm publish`

---

### Usual Commands for VUE CLI:

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
